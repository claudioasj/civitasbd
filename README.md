# BackEnd do civitas

Entre projeto foi desenvolvido baseado no trabalho de avaliação individual BackEnd.
<br><br>
<p align="center" >
<img src="./assets/civitaslogo.png">
<img src="./assets/letralogo.png">
</p>
<br><br>


temos nele um banco de dados com a seguinte modelagem:

<img src="./assets/modelagemCivitas.png">

- Nele podemos observar uma relaçao (1,n) do usuario(User) com uma oferta(Sale), logo é notavel que um usuario tem acesso a uma oferta ou mais.<br>
- Além disso, a Oferta possui uma relação (0,n) visto que ela pode ser apresentada a nenhum ou muitos usuarios.

## 📑 A tarefa exigia:
### ✅ As entidades devem ter 6 atributos.
- a declaração da User:
- ```javascript
    const User = sequelize.define('User', {
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false
        },

        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        date_of_birthday: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        cpf: {
            type: DataTypes.NUMBER,
            allowNull: false
        },
        phone_number: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        timestamps: false
    });
  ```
- a declaração da Sale:
- ```javascript
    const Sale = sequelize.define('Sale', {
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        quantity: {
            type: DataTypes.NUMBER,
            allowNull: false
        },
        store_id: {
            type: DataTypes.NUMBER,
            allowNull: false
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        post_date: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        limit_date: {
            type: DataTypes.DATEONLY
        }
        },{
        timestamps: false
        });
    ```
### ✅ Uma relação one to many
- que é exatamente a so User com a oferta.<br>
- foi declarada no Model `./src/models/User.js` como:
-  ```javascript
    User.associate = function(models) {
    User.belongsTo(models.Sale);
    }
    ```
### ✅ Retornar todas as instâncias de um objeto no BD
- Que foi usado uma INDEX para puxar todos os registro da tabela atraves do `.findAll()`.
- ```javascript
    const index = async(req,res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
    };
  ```
### ✅ Retornar informação de uma instância específica no BD
- Que foi usado uma SHOW para puxar de forma especifica por meio do `.findByPL(PRIMARY_NUMBER ou id)`.
- ```javascript
    const show = async(req,res) => {
        const {id} = req.params;
        try {
            const user = await User.findByPk(id);
            return res.status(200).json({user});
        }catch(err){
            return res.status(500).json({err});
        }
    };
    ```

### ✅ Editar uma instância específica no BD
- Foi feito com a UPDATE com  `.findByPL(PRIMARY_NUMBER ou id)`.
- ```javascript
        const update = async(req,res) => {
        const {id} = req.params;
        try {
            const [updated] = await User.update(req.body, {where: {id: id}});
            if(updated) {
                const user = await User.findByPk(id);
                return res.status(200).send(user);
            } 
            throw new Error();
        }catch(err){
            return res.status(500).json("Usuário não encontrado");
        }
    };
    ```


### ✅ Deletar uma instância específica no BD
- Para isso temos no projeto a DESTROY.
- ```javascript
        const destroy = async(req,res) => {
        const {id} = req.params;
        try {
            const deleted = await User.destroy({where: {id: id}});
            if(deleted) {
                return res.status(200).json("Usuário deletado com sucesso.");
            }
            throw new Error ();
        }catch(err){
            return res.status(500).json("Usuário não encontrado.");
        }
    };
    ```

### ✅ Inserir a relação de uma instância específica de um objeto com uma instância específica de outro objeto.
- que foi feito no controller do User : `./src/controllers/UserController.js`.
- ```javascript
    const addRelationship = async(req,res) => {
        const {id} = req.params;
        try {
            const instanceModel = await Model.findByPk(id);
            const instanceAnotherModel = await AnotherModel.findByPk(req.body.other_id);
            await instanceModel.setAnotherModel(instanceAnotherModel);
            return res.status(200).json(Model.findByPk(id));
        }catch(err){
            return res.status(500).json({err});
        }
    }
    ```
- e no controller da Sale:  `./src/controllers/SaleController.js`.
- ```javascript
    const addRelationSale = async(req,res) => {
        const {id} = req.params;
        try {
            const user = await User.findByPk(id);
            const sale = await Sale.findByPk(req.body.SaleId);
            await user.setSale(sale);
            return res.status(200).json(user);
        }catch(err){
            return res.status(500).json({err});
        }
    };
    ```

### ✅ Remover a relação de uma instância específica
- foi adicionado no controller da Sale:  `./src/controllers/SaleController.js`.
- ```javascript
    const removeRelationSale = async(req,res) => {
        const {id} = req.params;
        try {
            const user = await User.findByPk(id);
            await user.setSale(null);
            return res.status(200).json(user);
        }catch(err){
            return res.status(500).json({err});
        }
    };
    ```

> conclusão: Preciso confessar que fico meio perdido quando não consigo visualizar o que o código está fazendo, porem acredito que tenha realizado a tarefa da forma correta. 😁


<br><br>
<div align="center">
  <font color=#8A2BE2>
  Desenvolvido por Claudio Jr 🌊
  </font>
</div>


