const { response } = require('express');
const User = require('../models/User');
const Sales = require('../models/Sales');

//para criar usuario:
const create = async(req,res) => {
    try{
          const user = await User.create(req.body);
          return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});
      }catch(err){
          res.status(500).json({error: err});
      }
};

//para puxar todos os campos index e read
const index = async(req,res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};

//atualiza os datos
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

//para deletar
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }
};

//adicionando a relação 1-n
const addRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const instanceModel = await Model.findByPk(id);
        const instanceAnotherModel = await AnotherModel.findByPk(req.body.other_id);
        await instanceModel.setAnotherModel(instanceAnotherModel);
        return res.status(200).json(Model.findByPk(id));
    }catch(err){
        return res.status(500).json({err});
    }
}




//exportar as funções/ os metodos
module.exports = {
    index,
    show,
    create,
    update,
    destroy
};
