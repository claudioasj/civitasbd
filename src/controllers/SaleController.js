const { response } = require('express');
const User = require('../models/User');
const Sale = require('../models/Role');

const index = async(req,res) => {
    try {
        const sale = await Sale.findAll();
        return res.status(200).json({sale});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try{
          const sale = await Sale.create(req.body);
          return res.status(201).json({message: "Anuncio cadastrado com sucesso!", sale: sale});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const addRelationSale = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const sale = await Sale.findByPk(req.body.SaleId);
        await user.setSale(sale);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeRelationSale = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        await user.setSale(null);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
};

module.exports = {
    index,
    create,
};
