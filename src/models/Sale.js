const sequelize = require("../config/sequelize");
const DataTypes = require("sequelize");

const Sale = sequelize.define('Sale', {
  title: {
    type: DataTypes.STRING,
    allowNull: false
  },
  quantity: {
    type: DataTypes.NUMBER,
    allowNull: false
  },
  store_id: {
    type: DataTypes.NUMBER,
    allowNull: false
  },
  price: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  post_date: {
    type: DataTypes.DATEONLY,
    allowNull: false
  },
  limit_date: {
    type: DataTypes.DATEONLY
  }
},{
  timestamps: false
});

Sale.associate = function(models) {
  Role.hasMany(models.User);
}

module.exports = Sale;
