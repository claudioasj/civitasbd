// USer é uma loja!

const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

//definicao com os atributos
const User = sequelize.define('User', {
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    date_of_birthday: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    cpf: {
        type: DataTypes.NUMBER,
        allowNull: false
    },
    phone_number: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false
});

User.associate = function(models) {
    User.belongsTo(models.Sale);
}

module.exports = User;
